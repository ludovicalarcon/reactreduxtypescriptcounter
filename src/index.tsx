import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { count } from './reducers/index';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import Counter from './containers/counter';
import { Provider } from 'react-redux';

const store = createStore(count, {
  counter: 0,
}, (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
  <Provider store={store}>
    <Counter />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
