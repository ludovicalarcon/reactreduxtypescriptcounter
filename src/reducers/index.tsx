import { CounterAction } from '../actions';
import { StoreState } from '../types/index';
import { INCREMENT, DECREMENT } from '../constants/index';

export function count(state: StoreState, action: CounterAction): StoreState {
  switch (action.type) {
    case INCREMENT:
      console.log('Previous state:', state);
      console.log('Increment');
      const newIncState = { ...state, counter: state.counter + 1};
      console.log('New state:', newIncState);
      return newIncState;
    case DECREMENT:
    console.log('Previous state:', state);
    console.log('Decrement');
    const newDecState = { ...state, counter: Math.max(0, state.counter - 1) };
    console.log('New state:', newDecState);
    return newDecState;
  }
  return state;
}