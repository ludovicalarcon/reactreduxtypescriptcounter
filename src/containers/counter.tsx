import Counter from '../components/counter';
import * as actions from '../actions';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

export const mapStateToProps = ({ counter }: StoreState) => {
      return {
        counter
      }
}

export const mapDispatchToProps = (dispatch: Dispatch<actions.CounterAction>) => {
      return {
        onIncrement: () => {
          console.log('Dispatch Increment Action');
          return dispatch(actions.increment());
        },
        onDecrement: () => {
          console.log('Dispatch Decrement Action');
          dispatch(actions.decrement())
        },
      }
    }
    
export default connect(mapStateToProps, mapDispatchToProps)(Counter);