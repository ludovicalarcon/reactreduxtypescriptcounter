import * as constants from '../constants';

export interface Increment {
    type: constants.INCREMENT;
}

export interface Decrement {
    type: constants.DECREMENT;
}

export type CounterAction = Increment | Decrement;

export const increment = (): Increment => {
    return {
        type: constants.INCREMENT
    }
}

export const decrement = (): Decrement => {
      return {
          type: constants.DECREMENT
      }
  }