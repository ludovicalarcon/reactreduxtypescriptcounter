import * as React from 'react';

export interface Props {
      counter: number;
      onIncrement?: () => void;
      onDecrement?: () => void;
}

const Counter = ({ counter = 0, onIncrement, onDecrement }: Props) => {
  return (
    <div>
      <h1>{counter}</h1>
      <button onClick={onDecrement}>-</button>
      <button onClick={onIncrement}>+</button>
    </div>
  );
}

export default Counter;